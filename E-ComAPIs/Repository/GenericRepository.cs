﻿using E_ComAPIs.Entities;
using E_ComAPIs.IRepository;
using Microsoft.EntityFrameworkCore;

namespace E_ComAPIs.Repository
{
    public class GenericRepository<Entity> : IGenericReporsitory<Entity> where Entity : BaseEntity
    {
        protected DbSet<Entity> _dbSet;
        public GenericRepository(AppDbContext dbContext)
        {
            _dbSet = dbContext.Set<Entity>();
        }
        public async Task AddAsync(Entity entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task<List<Entity>?> GetAllAsync() => await _dbSet.AsNoTracking().ToListAsync();

        public Task<Entity?> GetById(int id) => _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public async Task<List<Entity>> Pagination(int pageIndex = 0, int pageSize = 10)
        {
            var result = await _dbSet.AsNoTracking()
                                     .OrderBy(x => x.Id)
                                     .Skip(pageIndex * pageSize)
                                     .Take(pageSize)
                                     .ToListAsync();

            return result;
        }

        public void SoftDelete(Entity entity)
        {
            entity.isDeleted = true;
        }

        public void Update(Entity entity)
        {
            _dbSet.Update(entity);
        }
    }
}
