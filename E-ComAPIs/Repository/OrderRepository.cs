﻿using E_ComAPIs.Entities;
using E_ComAPIs.IRepository;
using Microsoft.EntityFrameworkCore;

namespace E_ComAPIs.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<Order>?> GetOrderByCustomerId(int id)
        {
            return await _dbSet.Include(x => x.OrderItems)
                               .Where(x => x.CustomerId == id)
                               .ToListAsync();
        }
    }
}
