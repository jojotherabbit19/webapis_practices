﻿using E_ComAPIs.Entities;
using E_ComAPIs.IRepository;

namespace E_ComAPIs.Repository
{
    public class OrderItemsRepository : GenericRepository<OrderItems>, IOrderItemRepository
    {
        public OrderItemsRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
