﻿using E_ComAPIs.Entities;
using E_ComAPIs.IRepository;

namespace E_ComAPIs.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
