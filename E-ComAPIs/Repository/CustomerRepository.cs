﻿using E_ComAPIs.Entities;
using E_ComAPIs.IRepository;
using Microsoft.EntityFrameworkCore;

namespace E_ComAPIs.Repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
