﻿namespace E_ComAPIs.Entities
{
    public class OrderItems : BaseEntity
    {
        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
