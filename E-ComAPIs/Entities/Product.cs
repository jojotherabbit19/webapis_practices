﻿namespace E_ComAPIs.Entities
{
    public class Product : BaseEntity
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public ICollection<OrderItems>? OrderItems { get; set; }
    }
}
