﻿namespace E_ComAPIs.DTO
{
    public class OrderDTO
    {
        public DateTime OrderDate { get; set; }
        public decimal TotalPrice { get; set; }
        public int CustomerId { get; set; }
    }
}
