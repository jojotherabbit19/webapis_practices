﻿using E_ComAPIs.Entities;

namespace E_ComAPIs.IServices
{
    public interface IProductService
    {
        Task AddNewProduct(Product product);
        Task DeleteProduct(Product product);
        Task UpdateProduct(Product product);
        Task<Product?> GetProductById(int id);
        Task<List<Product>> GetProduct();
    }
}
