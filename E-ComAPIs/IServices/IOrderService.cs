﻿using E_ComAPIs.Entities;

namespace E_ComAPIs.IServices
{
    public interface IOrderService
    {
        Task AddNewOrder(Order order);
        Task DeleteOrder(int id);
        Task UpdateOrder(Order order);
        Task<Order?> GetOrder(int id);
        Task<Order?> GetOrderByCustomerId(int id);
    }
}
