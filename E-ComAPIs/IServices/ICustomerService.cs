﻿using E_ComAPIs.Entities;

namespace E_ComAPIs.IServices
{
    public interface ICustomerService
    {
        Task AddNewCustomer(Customer customer);
        Task DeleteCustomer(int id);
        Task UpdateCustomer(Customer customer);
        Task<Customer> GetCustomerById(int id);
        Task<List<Customer>?> GetAllCustomer();
        Task<List<Customer>?> CustomerFilter(string name, string address, string phone);
    }
}
