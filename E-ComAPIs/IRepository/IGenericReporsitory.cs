﻿namespace E_ComAPIs.IRepository
{
    public interface IGenericReporsitory<Entity>
    {
        Task AddAsync(Entity entity);
        void Update(Entity entity);
        void SoftDelete(Entity entity);
        Task<Entity?> GetById(int id);
        Task<List<Entity>?> GetAllAsync();
        Task<List<Entity>> Pagination(int pageIndex = 0, int pageSize = 10);
    }
}
