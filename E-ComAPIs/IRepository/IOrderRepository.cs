﻿using E_ComAPIs.Entities;

namespace E_ComAPIs.IRepository
{
    public interface IOrderRepository : IGenericReporsitory<Order>
    {
        Task<List<Order>?> GetOrderByCustomerId(int id);
    }
}
