﻿using ProductAPI.IRepository;
using ProductAPI.Repository;

namespace ProductAPI
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IProductRepository _productRepository;
        public UnitOfWork(AppDbContext context,
            IProductRepository productRepository)
        {
            _context = context;
            _productRepository = productRepository;
        }
        public IProductRepository ProductRepository => _productRepository;

        public async Task<int> SaveChangeAsync() => await _context.SaveChangesAsync();

    }
}
