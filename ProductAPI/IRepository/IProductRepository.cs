﻿using ProductAPI.Entities;

namespace ProductAPI.IRepository
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<List<Product>?> GetTop3Async();
    }
}
