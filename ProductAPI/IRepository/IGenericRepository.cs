﻿namespace ProductAPI.IRepository
{
    public interface IGenericRepository<Entity>
    {
        Task AddAsync(Entity entity);
        void Update(Entity entity);
        void Delete(Entity entity);
        Task<List<Entity>?> Pagination(int pageIndex = 0, int pageSize = 10);
        Task<Entity?> GetById(int id);
    }
}
