using Azure.Core;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using ProductAPI;
using ProductAPI.DTO;
using ProductAPI.IRepository;
using ProductAPI.IService;
using ProductAPI.Middlewares;
using ProductAPI.Repository;
using ProductAPI.Service;
using ProductAPI.Validation;

var builder = WebApplication.CreateBuilder(args);
{
    builder.Services.AddControllers();
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    // DbContext Service
    builder.Services.AddDbContext<AppDbContext>(option =>
        option.UseSqlServer(builder.Configuration.GetConnectionString("ProductDb"))
        );

    // add dependency injection
    builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
    builder.Services.AddScoped<IProductService, ProductService>();
    builder.Services.AddScoped<IProductRepository, ProductRepository>();
    builder.Services.AddScoped<IValidator<ProductDTO>, ProductValidation>();

    // add middleware services
    builder.Services.AddTransient<GlobalMiddleware>();
    builder.Services.AddHealthChecks();
}

var app = builder.Build();

{
    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    // use middleware

    app.UseMiddleware<GlobalMiddleware>();

    app.UseHealthChecks("/health");

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();

}