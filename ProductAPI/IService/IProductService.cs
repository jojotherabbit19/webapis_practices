﻿using ProductAPI.Entities;

namespace ProductAPI.IService
{
    public interface IProductService
    {
        Task<Product?> GetProduct(int id);
        Task AddProduct(Product product);
        Task UpdateProduct(Product product);
        Task<bool> DeleteProduct(int id);
        Task<List<Product>?> GetProducts(int pageIndex = 0, int pageSize = 10);
        Task<List<Product>?> GetTop3Products();
    }
}
