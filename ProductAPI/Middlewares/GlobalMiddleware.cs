﻿using System.Net;

namespace ProductAPI.Middlewares
{
    public class GlobalMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                await context.Response.WriteAsync($"Error: {ex.Message}");
                // add Log
            }
        }
    }
}
