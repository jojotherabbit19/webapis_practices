﻿using ProductAPI.Entities;
using ProductAPI.IService;

namespace ProductAPI.Service
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task AddProduct(Product product)
        {
            await _unitOfWork.ProductRepository.AddAsync(product);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Add Succeed");
            }
        }

        public async Task<bool> DeleteProduct(int id)
        {
            var product = await _unitOfWork.ProductRepository.GetById(id);
            if (product is null)
            {
                Console.WriteLine($"Not found product ID {id}");
                return false;
            }
            _unitOfWork.ProductRepository.Delete(product);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Delete Succeed");
            }

            return true;
        }

        public async Task<Product?> GetProduct(int id) => await _unitOfWork.ProductRepository.GetById(id);

        public async Task<List<Product>?> GetProducts(int pageIndex = 0, int pageSize = 10)
            => await _unitOfWork.ProductRepository.Pagination(pageIndex, pageSize);

        public async Task<List<Product>?> GetTop3Products()
            => await _unitOfWork.ProductRepository.GetTop3Async();

        public async Task UpdateProduct(Product product)
        {
            _unitOfWork.ProductRepository.Update(product);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Update Succeed");
            }
        }
    }
}
