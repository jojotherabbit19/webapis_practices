﻿using Microsoft.EntityFrameworkCore;
using ProductAPI.Entities;

namespace ProductAPI
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                        .Property(x => x.Price)
                        .HasColumnType("money");
        }
    }
}
