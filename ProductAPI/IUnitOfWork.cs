﻿using ProductAPI.IRepository;

namespace ProductAPI
{
    public interface IUnitOfWork
    {
        IProductRepository ProductRepository { get; }
        Task<int> SaveChangeAsync();
    }
}
