﻿using FluentValidation;
using ProductAPI.DTO;

namespace ProductAPI.Validation
{
    public class ProductValidation : AbstractValidator<ProductDTO>
    {
        public ProductValidation()
        {
            RuleFor(x => x.Name)
                    .NotEmpty()
                    .NotNull()
                    .WithMessage("Name cant be null or empty.");

            RuleFor(x => x.Price)
                    .NotEmpty()
                    .NotNull()
                    .GreaterThan(0);
        }
    }
}
