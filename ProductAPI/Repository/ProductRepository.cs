﻿using Microsoft.EntityFrameworkCore;
using ProductAPI.Entities;
using ProductAPI.IRepository;

namespace ProductAPI.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<Product>?> GetTop3Async()
            => await dbSet.OrderByDescending(x => x.Price).Take(3).AsNoTracking().ToListAsync();
    }
}
