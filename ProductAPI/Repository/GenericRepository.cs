﻿using Microsoft.EntityFrameworkCore;
using ProductAPI.Entities;
using ProductAPI.IRepository;

namespace ProductAPI.Repository
{
    public class GenericRepository<Entity> : IGenericRepository<Entity> where Entity : BaseEntity
    {
        protected DbSet<Entity> dbSet;
        public GenericRepository(AppDbContext dbContext)
        {
            dbSet = dbContext.Set<Entity>();
        }
        public async Task AddAsync(Entity entity)
        {
            await dbSet.AddAsync(entity);
        }

        public void Delete(Entity entity)
        {
            entity.IsDeleted = true;
            dbSet.Update(entity);
        }

        public async Task<List<Entity>?> Pagination(int pageIndex = 0, int pageSize = 10)
            => await dbSet.AsNoTracking()
                          .OrderByDescending(x => x.Id)
                          .Skip(pageIndex * pageSize)
                          .Take(pageSize)
                          .ToListAsync();

        public async Task<Entity?> GetById(int id)
            => await dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public void Update(Entity entity)
        {
            dbSet.Update(entity);
        }
    }
}
