﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using ProductAPI.DTO;
using ProductAPI.Entities;
using ProductAPI.IService;

namespace ProductAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly IValidator<ProductDTO> _validator;
        public ProductController(IProductService productService,
            IValidator<ProductDTO> validator)
        {
            _productService = productService;
            _validator = validator;
        }

        [HttpPost]
        public async Task<IActionResult> AddProductAsync([FromBody] ProductDTO product)
        {
            var result = _validator.Validate(product);
            if (result.IsValid)
            {
                var productObj = new Product()
                {
                    Name = product.Name,
                    Price = product.Price,
                };
                await _productService.AddProduct(productObj);
                return Ok(productObj);
            }

            return BadRequest("Add Fail");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProductAsync([FromRoute] int id, [FromBody] ProductDTO product)
        {
            var productObj = await _productService.GetProduct(id);
            if (productObj is not null)
            {
                var result = _validator.Validate(product);
                if (result.IsValid)
                {
                    // assign update value
                    productObj.Name = product.Name;
                    productObj.Price = product.Price;

                    await _productService.UpdateProduct(productObj);
                    return Ok(productObj);
                }
            }

            return BadRequest("Update Fail");
        }

        [HttpGet]
        public async Task<IActionResult> GetProductsAsync(int pageIndex = 0, int pageSize = 10)
        {
            var products = await _productService.GetProducts(pageIndex, pageSize);
            return Ok(products);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var result = await _productService.DeleteProduct(id);
            if (result)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}
